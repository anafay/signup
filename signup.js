var signUpViewModel = function(email,passcode){
	this.userName = ko.observable(email).extend({required: "Please Enter a valid email address"});
	this.passWord = ko.observable(passcode);
	this.userNameCopy = ko.observable(email);
	this.passWordCopy = ko.observable(passcode);
    
	this.checkUserName = ko.computed(function(){
		return this.userName() != this.userNameCopy();
	},this);
	this.checkPassword = ko.computed(function(){
		return this.passWord() != this.passWordCopy();
	},this);
};

ko.extenders.required = function(target,message){

    target.hasError = ko.observable();
    target.validationMessage = ko.observable();
    var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    function validate(newValue) {
    	console.log(pattern);
       target.hasError((newValue.match(pattern)!=null) ? false : true);
       target.validationMessage((newValue.match(pattern)!=null) ? "" : message);
    }
 
    validate(target());

    target.subscribe(validate);
 
    return target;
};

